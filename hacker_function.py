#!/usr/bin/python3

import requests
from prettytable import PrettyTable

api_url = "https://hacker-news.firebaseio.com/v0/beststories.json?&orderBy=%22$key%22&limitToFirst=10"
api_url_id = "https://hacker-news.firebaseio.com/v0/item/"


def table_creation():
    x = PrettyTable()
    x.field_names = ["Title","URL"]
    x.align["URL"] = "l"
    x.align["Title"] = "r"
    x._max_width = {"Title" : 80, "URL" : 80}
    return x

def print_table(x,title,url):
    x.add_row([title,url])
    return x 


def api_call(api_url,api_url_id):
    response = requests.get(api_url).json()
    table_creation_variable = table_creation()
    for r in response:
            try:
                link = f"{api_url_id}{r}.json"
                response = requests.get(link)
                data = response.json()
                title = data['title']
                url = data['url']
                print_table_variabl = print_table(table_creation_variable,title,url)
            except KeyError:
                pass
    return print_table_variabl        

def main():
    value = api_call(api_url,api_url_id)
    print(value)

main()    
                             
