#!/usr/bin/python3

import requests
from prettytable import PrettyTable

def api_call():

def print_table():

x = PrettyTable()
response = requests.get("https://hacker-news.firebaseio.com/v0/beststories.json?&orderBy=%22$key%22&limitToFirst=10").json()
for r in response:
        link = f"https://hacker-news.firebaseio.com/v0/item/{r}.json"
        response = requests.get(link)
        data = response.json()
        title = data['title']
        url = data['url']
        x.field_names = ["Title","URL"]
        x.align["URL"] = "l"
        x.align["Title"] = "r"
        x.add_row([title,url])

print(x)
