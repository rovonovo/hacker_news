#!/usr/bin/python3

import requests
from prettytable import PrettyTable

api_url = "https://hacker-news.firebaseio.com/v0/beststories.json?&orderBy=%22$key%22&limitToFirst=10"
api_url_id = "https://hacker-news.firebaseio.com/v0/item/"
x = PrettyTable()

def print_table(title,url):
    x.field_names = ["Title","URL"]
    x.align["URL"] = "l"
    x.align["Title"] = "r"
    x.add_row([title,url])

def api_call(api_url,api_url_id):
    response = requests.get(api_url).json()
    for r in response:
            link = f"{api_url_id}{r}.json"
            response = requests.get(link)
            data = response.json()
            title = data['title']
            url = data['url']
            print_table(title,url)
    print(x)

api_call(api_url,api_url_id)
